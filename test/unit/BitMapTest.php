<?php


namespace Test\unit;


use PHPUnit\Framework\TestCase;
use PrimeNumbers\BitPosition;

class BitMapTest extends TestCase
{
    public function testSetGet() {
        $bitMap = new \PrimeNumbers\Bitmap();
        $bitMap->set(4);
        self::assertTrue($bitMap->get(4));

        $bitMap->set(100);
        self::assertTrue($bitMap->get(100));
    }

    public function testSaveBinGetBin() {
        $bitMap = new \PrimeNumbers\Bitmap();
        $bitMap->saveBin('1000', 0);
        self::assertEquals('10000000000000000000000000000000', $bitMap->getBin(0));

        $bitMap = new \PrimeNumbers\Bitmap();
        $bitMap->saveBin('1001', 3);
        self::assertEquals('10010000000000000000000000000000', $bitMap->getBin(3));

        $bitMap = new \PrimeNumbers\Bitmap();
        $bitMap->saveBin('001', 3);
        self::assertEquals('00100000000000000000000000000000', $bitMap->getBin(3));
    }
}