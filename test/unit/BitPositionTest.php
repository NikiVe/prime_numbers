<?php


namespace Test\unit;


use PHPUnit\Framework\TestCase;
use PrimeNumbers\BitPosition;

class BitPositionTest extends TestCase
{
    public function testPosition()
    {
        $bitPos = new BitPosition(33);
        self::assertEquals(1, $bitPos->getFloor());
        self::assertEquals(1, $bitPos->getPosition());

        $bitPos = new BitPosition(0);
        self::assertEquals(0, $bitPos->getFloor());
        self::assertEquals(0, $bitPos->getPosition());

        $bitPos = new BitPosition(100);
        self::assertEquals(3, $bitPos->getFloor());
        self::assertEquals(4, $bitPos->getPosition());
    }
}