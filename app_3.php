<?php

use Tester\Tester;

include __DIR__ . '/vendor/autoload.php';

// На 11 тесте дефолтный набор памяти в PHP переполняется
ini_set('memory_limit', '-1');

Tester::start(__DIR__ . '/source/', new PrimeNumbers\Eratosthenes());
