<?php


namespace PrimeNumbers;


use Tester\TestFunc;

/**
 * Class Eratosthenes
 * @package PrimeNumbers
 *
 * Алгоритм Эратосфена
 */
class Eratosthenes implements TestFunc
{
    public function run(string $values): string
    {
        $n = (int)$values;
        $a = self::init($n);

        for ($i = 2; $i ** 2 <= $n; $i++) {
            if ($a[$i] === true) {
                $j = $i ** 2;

                $stage = 1;
                while ($j <= $n) {
                    $a[$j] = false;

                    $j = $i ** 2 + $i * $stage;

                    $stage++;
                }
            }
        }
        return count(
            array_filter(
                $a,
                static function ($v) {return $v === true;})
        );
    }

    public static function init(int $n) : array {
        $array = [];
        for ($i = 2; $i <= $n; $i++) {
            $array[$i] = true;
        }
        return $array;
    }
}