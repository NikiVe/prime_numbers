<?php


namespace PrimeNumbers;


use Tester\TestFunc;

/**
 * Class EratosthenesBitOptimization
 * @package PrimeNumbers
 *
 * Алгоритм Эратосфена с использованием битовой карты
 */
class EratosthenesBitOptimization implements TestFunc
{
    public function run(string $values): string
    {
        $n = (int)$values;
        $a = Bitmap::init($n);

        for ($i = 2; $i ** 2 <= $n; $i++) {
            if ($a->get($i - 2) === true) {
                $j = $i ** 2;

                $stage = 1;
                while ($j <= $n) {
                    $a->set($j - 2, false);

                    $j = $i ** 2 + $i * $stage;

                    $stage++;
                }
            }
        }
        return $this->getCount($a, $n);
    }

    protected function getCount(Bitmap $bitmap, int $n) {
        $bin = '';
        $binArray = $bitmap->getBitmap();
        foreach ($binArray as $floor) {
            $bin .= $floor;
        }

        $bin = substr($bin, 0, $n - 1);
        return substr_count($bin, '1');
    }
}