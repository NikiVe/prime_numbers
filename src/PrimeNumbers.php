<?php


namespace PrimeNumbers;


use Brick\Math\BigInteger as Integer;
use Tester\TestFunc;

/**
 * Class PrimeNumbers
 * @package PrimeNumbers
 *
 * Простой перебор делителей
 */
class PrimeNumbers implements TestFunc
{
    public function run(string $values) : string {
        $n = Integer::of($values);

        $count = Integer::zero();

        for($i = Integer::of(2); $n->isGreaterThanOrEqualTo($i); $i = $i->plus(Integer::one())) {
            $count = $count->plus(self::isPrime($i) ? Integer::one() : Integer::zero());
        }
        return $count->toInt();
    }

    public static function isPrime(Integer $value) {
        $count = Integer::zero();

        for ($i = Integer::one(); $value->isGreaterThanOrEqualTo($i); $i = $i->plus(Integer::one())) {
            if ($value->mod($i)->isZero()) {
                $count = $count->plus(Integer::one());
            }
        }
        return $count->isEqualTo(Integer::of(2));
    }
}