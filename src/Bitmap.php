<?php


namespace PrimeNumbers;


class Bitmap
{
    protected $array = [];

    public function __construct() {
    }

    public function get(int $p) {
        $position = new BitPosition($p);

        $bin = $this->getBin($position->getFloor());

        return $bin[$position->getPosition()] === "1";
    }

    public function set(int $p, bool $b = true) {
        $position = new BitPosition($p);

        $bin = $this->getBin($position->getFloor());
        $bin[$position->getPosition()] = $b ? '1' : '0';

        $this->saveBin($bin, $position->getFloor());
    }

    public function saveBin(string $bin, int $floor) {
        $this->array[$floor] = bindec(strrev($bin));
    }

    public function getBin(int $floor) {
        if (!isset($this->array[$floor])) {
            $this->array[$floor] = '';
        }

        $bin = strrev(decbin($this->array[$floor]));
        for ($i = strlen($bin); $i < 32; $i++) {
            $bin[$i] = '0';
        }
        return $bin;
    }

    public function getBitmap() {
        $bitmap = [];
        foreach ($this->array as $floor => $val) {
            $bitmap[$floor] = $this->getBin($floor);
        }
        return $bitmap;
    }

    public static function init(int $n) {
        $p = new BitPosition($n);
        $bitmap = new self();

        for ($i = 0; $i < $p->getFloor(); $i++) {
            $bitmap->array[$i] = 2 ** 32 - 1;
        }

        $bitmap->array[$p->getFloor()] = 2 ** $p->getPosition() - 1;

        return $bitmap;
    }
}