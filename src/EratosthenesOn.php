<?php


namespace PrimeNumbers;


use Brick\Math\BigInteger;
use Tester\TestFunc;

/**
 * Class EratosthenesOn
 * @package PrimeNumbers
 *
 * Алгоритм Эратосфена со сложностью O(n)
 */
class EratosthenesOn implements TestFunc
{
    public function run(string $values) : string {
        $n = BigInteger::of($values);

        $lp = [];
        $pr = [];

        for($i = BigInteger::of(2); $n->isGreaterThanOrEqualTo($i); $i = $i->plus(BigInteger::one())) {
            if (!isset($lp[$i->toInt()])) {
                $num = BigInteger::of($i);

                $lp[$i->toInt()] = $num;
                $pr[]   = $num;
            }

            /* @var BigInteger $p */
            foreach ($pr as $p) {
                if ($lp[$i->toInt()]->isGreaterThanOrEqualTo($p) && $n->isGreaterThanOrEqualTo($p->multipliedBy($i))) {
                    $lp[$p->multipliedBy($i)->toInt()] = $p;
                } else {
                    continue 2;
                }
            }
        }
        return count($pr);
    }

}