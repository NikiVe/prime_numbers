<?php


namespace PrimeNumbers;


use Brick\Math\BigInteger as Integer;
use Tester\TestFunc;

/**
 * Class PrimeNumbersOptinization
 * @package PrimeNumbers
 *
 * Оптимизированный перебор
 */
class PrimeNumbersOptimization implements TestFunc
{
    protected array $arPrime = [];

    public function run(string $values) : string {
        $this->arPrime = [];

        $n = Integer::of($values);

        $count = Integer::zero();

        for($i = Integer::of(2); $n->isGreaterThanOrEqualTo($i); $i = $i->plus(Integer::one())) {
            if ($this->isPrime($i)) {
                $count = $count->plus(Integer::one());
                $this->arPrime[] = $i;
            }
        }
        return $count->toInt();
    }

    public function isPrime(Integer $value) {
        if ($value->isEqualTo(Integer::of(2))) {
            return true;
        }

        if ($value->mod(2)->isEqualTo(Integer::zero())) {
            return false;
        }

        foreach ($this->arPrime as $prime) {
            if ($value->mod($prime)->isEqualTo(Integer::zero())) {
                return false;
            }
        }
        return true;
    }
}