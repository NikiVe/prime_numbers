<?php


namespace PrimeNumbers;


class BitPosition
{
    protected $floor;
    protected $position;

    public function __construct(int $p) {
        $this->floor = (int) ceil(($p + 1) / 32) - 1;
        $this->position = $p - 32 * ($this->floor);
    }

    public function getFloor() {
        return $this->floor;
    }

    public function getPosition() {
        return $this->position;
    }
}